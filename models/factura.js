'use strict';
module.exports = (sequalize, DataTypes) => {
    const factura = sequalize.define('factura', {
          external_key: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            allowNull: false
          },

          tipo: {
            type: DataTypes.ENUM("VENTA","MANTENIMIENTO"),
            allowNull: false,
            defaultValue: "VENTA"
          },
          
          venta_data: {
            type: DataTypes.JSON,
            allowNull: false
          }
      });
      factura.associate = function(models) {
        factura.belongsTo(models.venta, {foreignKey: 'id_venta'});
        factura.belongsTo(models.mantenimiento, { foreignKey: 'id_mantenimiento' });
      }
    return factura;
}