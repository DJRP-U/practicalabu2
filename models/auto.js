'use strict';
module.exports = (sequalize, DataTypes) => {
    const auto = sequalize.define('auto', {

        modelo: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: "indefinido"
        },
        color: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: 'indefinido',
        },
        anio: {
            type: DataTypes.INTEGER,
            allowNull: true,
            defaultValue: 0
        },
        external_id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4},
        tipo: {
            type: DataTypes.ENUM("4X4","2X$"),
            allowNull: true,
            defaultValue: "2X$"
        },
        motor: {
            type: DataTypes.ENUM("GASOLINA","DIESEL","ELECTRICO","HIDROGENO"),
            allowNull: true,
            defaultValue:"GASOLINA"
        },
        detalles: {
            type: DataTypes.STRING(200),
            allowNull: true,
            defaultValue: "Sin descripcion"
        },
        precio: {
            type: DataTypes.DOUBLE,
            allowNull: false,
            defaultValue: 0
        }
    }, {freezeTableName: true});
    auto.associate = function(models) {
        auto.hasMany(models.venta, { foreignKey: 'id_auto' });
        auto.hasMany(models.mantenimiento, { foreignKey: 'id_auto' });
      };
    return auto;
}