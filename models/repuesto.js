module.exports = (sequalize, DataTypes) => {
    const repuesto = sequalize.define('repuesto', {


        external_id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4},

        nombres: {
            type: DataTypes.STRING(50),
            defaultValue: "Sin Datos"
        },

        precio_unitario: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        
        detalles: {
            type: DataTypes.STRING(200),
            allowNull: true,
            defaultValue: "Sin descripcion"
        }

        

    }, {freezeTableName: true});
        repuesto.associate = function(models) {
        repuesto.belongsTo(models.mantenimiento, { foreignKey: 'id_mantenimiento' });
        };
    return repuesto;
}