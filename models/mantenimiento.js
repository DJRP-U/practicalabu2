'use strict';
module.exports = (sequalize, DataTypes) => {
    const mantenimiento = sequalize.define('mantenimiento', {

        external_id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4},
        
        precio_total: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        iva: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        precio_final: {
            type: DataTypes.DOUBLE,
            allowNull: false
        },
        detalles: {
            type: DataTypes.STRING(200),
            allowNull: true,
            defaultValue: "Sin descripcion"
        }


        

    }, {freezeTableName: true});
    mantenimiento.associate = function(models) {
        mantenimiento.hasMany(models.repuesto, { foreignKey: 'id_mantenimiento' });
        mantenimiento.hasMany(models.factura, { foreignKey: 'id_mantenimiento' });
        mantenimiento.belongsTo(models.auto, {foreignKey: 'id_auto'});
        mantenimiento.belongsTo(models.persona, {foreignKey: 'id_persona'});
      };
    return mantenimiento;
}