'use strict';
module.exports = (sequalize, DataTypes) => {
    const cuenta = sequalize.define('cuenta', {

        Usuario: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        Contrasenia: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true},
        Email: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {freezeTableName: true});
    cuenta.associate = function(models) {
        cuenta.belongsTo(models.persona, {foreignKey: 'id_persona'});
    }
    return cuenta;
}