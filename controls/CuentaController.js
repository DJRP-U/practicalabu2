'use strict';
const { body, validationResult, check } = require('express-validator');
const { Op, sequelize } = require('sequelize');
var models = require('../models/');
const bcrypt = require('bcrypt');
var cuenta = models.cuenta;
let jwt = require('jsonwebtoken');
class CuentaController{
    async listar(req, res) {
        var listar = await cuenta.findAll({
            attributes: ['Usuario','Email']
        });
        res.status(200);
        res.json({ msg: "Bienvenido!", code: 200, info: listar });

    }
    
    async sesion(req,res){
        let errors = validationResult(req, res);
            if (errors.isEmpty()) {
                var login = await cuenta.findOne({
                    where: {
                        Email: req.body.correo
                    }
                });
                if (login === null) {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400, error: errors });
                } else{
                    res.status(200);
                    var isClaveValida = function(clave, claveUser){
                        return bcrypt.compareSync(claveUser, clave);
                    };

                    if (login.estado) {
                        if (isClaveValida(login.Contrasenia, req.body.clave)) {
                            const tokenData = {
                                external: login.external_id,
                                Email: login.Email,
                                check: true
                            };
                            require('dotenv').config();
                            const llave = process.env.KEY
                            const token = jwt.sign(tokenData,llave,{
                                expiresIn:'1h'
                            });
                            res.json({
                                msg: 'OK!',
                                code: 200,
                                validToken: token
                                })

                        } else {
                            res.status(400);
                            res.json({ msg: "Clave incorrecta", code: 400, error: errors });
                        }
                    } else {
                        res.status(400);
                        res.json({ msg: "Cuenta desactivada", code: 400, error: errors });
                    }
                   
                }
            }else{
                res.status(400);
                res.json({ msg: "Datos no encontrados", code: 400, error: errors });
            }
    }
}
module.exports = CuentaController;