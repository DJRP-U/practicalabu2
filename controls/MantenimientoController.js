'use strict';
const { body, validationResult, check } = require('express-validator');
const { Op, sequelize } = require('sequelize');
const iva = 0.12;
var models = require('../models/');
var persona = models.persona;
var venta = models.venta;
var auto = models.auto;
var marca = models.marca;
var repuesto = models.repuesto;
const bcrypt = require('bcrypt');
const mantenimiento = models.mantenimiento;
const factura = models.factura;
const salRounds = 8;
class MantenimientoController {
    async listar(req, res) {
        try {
            const listar = await mantenimiento.findAll({
                attributes: ['external_id','precio_total','detalles']
            });
            res.status(200);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener los mantenimientos realizados' });
        }
    }

    async listarRepuestos(req, res) {
        try {
            const listar = await mantenimiento.findAll({
                attributes: ['nombre','external_id','precio_unitario','detalles']
            });
            res.status(200);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener los repuestos r' });
        }
    }

    async obtener(req, res) {
        const external = req.params.external;
        try {findAll
            var lista = await mantenimiento.findOne({
                where: { external_id: external },
                attributes: ['external_id','precio_total','detalles']
            });
            if (lista == null) {
                lista = {
                    msg: "Sin venta"
                };
            }
            res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener el mantenimiento solicitado' });
        }
    }


    async guardar(req, res) {
                let errors = validationResult(req);
                if (errors.isEmpty()) {
                    const personaId= req.body.external_persona;
                    const autoId= req.body.external_auto;
                    const external_repuestos = req.body.external_repuestos;
    
    
                    if (1===1) {
                        let personaAux = await persona.findOne({ where: { external_id: personaId } });
                        let autoAux = await auto.findOne({ where: { external_id: autoId } });
                        let repuestos = await repuesto.findAll({
                            where: {
                                external_id: {
                                    [Op.in]: external_repuestos
                                }
                            }
                        })
    
                        if (repuestos) {
                            //data arreglo asociativo= es un direccionario = clave:valor
                            const valor_total = repuestos.reduce((sum,repuesto) => sum + parseFloat(repuesto.precio_unitario),0);
                            const auxIva = parseFloat(valor_total) * iva;
                            const precioFinal = parseFloat(valor_total) + auxIva;
                            var data = {
                                precio_total: valor_total,
                                iva: auxIva,
                                precio_final: precioFinal,
                                id_auto: autoAux.id,
                                id_persona: personaAux.id
                            }
                            try {
                                const mantCreada = await mantenimiento.create(data);

                                try {
                                    var dataFact = {
                                        //datos de la persona
                                        nombres: personaAux.nombres,
                                        apellidos: personaAux.apellidos,
                                        identificacion: personaAux.identificacion,
                                        correo: personaAux.correo,
                                        direccion: personaAux.direccion,
                                        
                                        //Datos del mantenimiento
                                        detalles: mantCreada.detalles,
                                        repuestos: repuestos,
                                        precio_total: valor_total,
                                        iva: auxIva,
                                        precio_final: precioFinal,
                                    };
                                    var datosFactura = {
                                        venta_data : JSON.stringify(dataFact),
                                        id_mantenimiento: mantCreada.id,
                                        tipo: "MANTENIMIENTO"
                                    }
                                    await factura.create(datosFactura);
                                    res.json({ msg: "Se han registrado los datos de mantenimiento", code: 200 });
                                } catch (error) {
                                    console.error(error);
                                    res.status(500).json({ msg: 'Error al guardar la factura', code: 500 });
                                }
                            } catch (error) {
                                if (error.errors && error.errors[0].message) {
                                    res.status(400);
                                    res.json({ msg: error.errors[0].message, code: 400 });
                                } else {
                                    res.status(400);
                                    res.json({ msg: error.message, code: 400 });
                                }
                            }
                        } else {
                            res.status(400);
                            res.json({ msg: "Datos no encontrados", code: 400 });
                        }
                    } else {
                        res.status(400);
                        res.json({ msg: "Faltan datos", code: 400 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Datos faltantes", code: 400, errors: errors });
                }
            
    }


}
module.exports = MantenimientoController;