'use strict';
const { body, validationResult, check } = require('express-validator');
const { Op, sequelize } = require('sequelize');
const iva = 0.12;
var models = require('../models/');
var persona = models.persona;
var venta = models.venta;
var auto = models.auto;
var marca = models.marca;
const bcrypt = require('bcrypt');
const factura = models.factura;
const salRounds = 8;
class VentaController {
    async listar(req, res) {
        try {
            const listar = await venta.findAll({
                attributes: ['external_id','precio_unitario','iva','precio_total'],
                include:[
                    {
                        model:auto,
                        attributes:['modelo','precio']
                    },
                    {
                        model:persona,
                        attributes:['identificacion','nombres']
                    }
                ]
            })
            res.status(200);
            res.json({ msg: "0k", code: 200, info: listar });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener las ventas disponibles' });
        }
    }

    async obtener(req, res) {
        const external = req.params.external;
        try {findAll
            var lista = await venta.findOne({
                where: { external_id: external },
                attributes: ['external_id','precio_unitario','iva','precio_total']
            });
            if (lista == null) {
                lista = {
                    msg: "Sin venta"
                };
            }
            res.status(200);
            res.json({ msg: "OK!", code: 200, info: lista });
        } catch (error) {
            console.error(error);
            res.status(500).json({ message: 'Error al obtener la venta solicitada' });
        }
    }


    async guardar(req, res) {
        
            let errors = validationResult(req);
            if (errors.isEmpty()) {
                const autoId= req.body.external_auto;
                const personaId= req.body.external_persona;


                if (1===1) {
                    let autoAux = await auto.findOne({ where: { external_id: autoId } });
                    let personaAux = await persona.findOne({ where: { external_id: personaId } });

                    if (autoAux) {
                        //data arreglo asociativo= es un direccionario = clave:valor
                        const auxIva = parseFloat(autoAux.precio) * iva;
                        const precioFinal = parseFloat(autoAux.precio) + auxIva;
                        var data = {
                            precio_unitario: autoAux.precio,
                            iva: auxIva,
                            precio_total: precioFinal,
                            id_auto: autoAux.id,
                            id_persona: personaAux.id
                        }
                        try {
                            const ventaCreada = await venta.create(data);
                            res.json({ msg: "Se han registrado los datos de venta", code: 200 });
                            try {
                                var dataFact = {
                                    //datos de la persona
                                    nombres: personaAux.nombres,
                                    apellidos: personaAux.apellidos,
                                    identificacion: personaAux.identificacion,
                                    correo: personaAux.correo,
                                    direccion: personaAux.direccion,

                                    //datos del auto
                                    modelo: autoAux.modelo,
                                    color: autoAux.color,
                                    anio: autoAux.anio,
                                    tipo: autoAux.tipo,
                                    detalles: autoAux.detalles,
                                    
                                    //Datos de la venta
                                    precio_unitario: autoAux.precio,
                                    iva: auxIva,
                                    precio_total: precioFinal,
                                };
                                var datosFactura = {
                                    venta_data : JSON.stringify(dataFact),
                                    id_venta: ventaCreada.id
                                }
                                await factura.create(datosFactura);
                            } catch (error) {
                                console.error(error);
                                res.status(500).json({ msg: 'Error al guardar la factura', code: 500 });
                            }
                        } catch (error) {
                            if (error.errors && error.errors[0].message) {
                                res.status(400);
                                res.json({ msg: error.errors[0].message, code: 400 });
                            } else {
                                res.status(400);
                                res.json({ msg: error.message, code: 400 });
                            }
                        }
                    } else {
                        res.status(400);
                        res.json({ msg: "Datos no encontrados", code: 400 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Faltan datos", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Datos faltantes", code: 400, errors: errors });
            }
    }


}
module.exports = VentaController;